//region Refsheet class
function Refsheet() {
    var _this = this;

    _this.viewConfig = {
        language: "",
        languages: {},
        template: "",
        templates: {},
        classification: "",
        classifications: {}
    };

    _this.generateHash = function (language, template, classification) {
        if (!language) language = _this.viewConfig.language;
        if (!template) template = _this.viewConfig.template;
        if (!classification) classification = _this.viewConfig.classification;
        return language + "-" + template + "-" + classification;
    };

    _this.parseHash = function () {
        try {
            var hash = document.location.hash, getAt = function (array, index) {
                return array.hasOwnProperty(index) ? array[index] : null;
            };
            if (hash) {
                hash = hash.substr(1).split("-");
                var
                    language = getAt(hash, 0),
                    template = getAt(hash, 1),
                    classification = getAt(hash, 2);
                if(_this.viewConfig.languages && _this.viewConfig.languages.hasOwnProperty(language)) {
                    _this.viewConfig.language =  language;
                }
                if(_this.viewConfig.templates && _this.viewConfig.templates.hasOwnProperty(template)) {
                    _this.viewConfig.template = template;
                }
                if(_this.viewConfig.classifications && _this.viewConfig.classifications.hasOwnProperty(classification)) {
                    _this.viewConfig.classification = classification;
                }
            }
            _this.switchLanguage(_this.viewConfig.language);
            _this.switchTemplate(_this.viewConfig.template);
            _this.switchClassification(_this.viewConfig.classification);
        }
        catch(e) {
            console.log(e);
        }
    };

    _this.getSwitchArea = function (id, target) {
        if (!id) id = "switch_area";
        if (!target) target = $("body");
        var switchArea = target.find("#" + id);
        if (switchArea.length == 0) {
            target.append('<div id="' + id + '"></div>');
            switchArea = _this.getSwitchArea(id, target);
        }
        return switchArea;
    };

    _this.contentSwitch = function(classPrefix, className) {
        var allElements = $("[class^=\"" + classPrefix + "\"], [class*=\"" + classPrefix + "\"]"),
            elementsToShow = $("." + className);
        allElements.addClass("hidden");
        elementsToShow.removeClass("hidden");
    };

    _this.insertSwitchElement = function(id, values, defaultValue, callback) {
        var switches = _this.getSwitchArea(),
            thisSwitch = _this.getSwitchArea(id, switches),
            options, o;
        options = "";
        for (o in values) {
            if (values.hasOwnProperty(o)) {
                options = options + '<option value="' + o + '">' + values[o] + '</option>';
            }
        }
        thisSwitch.html('<select>' + options + '</select>');
        thisSwitch.find("select").on("change", function () {
            callback($(this).val());
            document.location.hash = _this.generateHash();
        }).val(defaultValue);
    };

    _this.insertLanguageSwitch = function () {
        _this.insertSwitchElement(
            "switch_lang",
            _this.viewConfig.languages,
            _this.viewConfig.language,
            _this.switchLanguage
        );
    };
    _this.switchLanguage = function (language) {
        _this.contentSwitch("lang_", "lang_" + language);
        _this.viewConfig.language = language;
    };

    _this.insertTemplateSwitch = function () {
        _this.insertSwitchElement(
            "switch_template",
            _this.viewConfig.templates,
            _this.viewConfig.template,
            _this.switchTemplate
        );
    };
    _this.activeTemplate = {
        className: ""
    };
    _this.switchTemplate = function (template) {
        var body = $("body");
        if (_this.activeTemplate.className) {
            body.toggleClass(_this.activeTemplate.className);
        }
        _this.activeTemplate.className = "style_" + template;
        body.toggleClass(_this.activeTemplate.className);
        _this.viewConfig.template = template;
    };

    _this.insertClassificationSwitch = function () {
        _this.insertSwitchElement(
            "switch_classification",
            _this.viewConfig.classifications,
            _this.viewConfig.classification,
            _this.switchClassification
        );
    };
    _this.classification = {
        className: "",
        elements: []
    };
    _this.switchClassification = function (classification) {
        _this.contentSwitch("classification_", "classification_" + classification);
        _this.viewConfig.classification = classification;
    };
}
//endregion

//region Google Analytics
(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

//noinspection JSUnresolvedFunction
ga('create', 'UA-1473207-8', 'auto');
//noinspection JSUnresolvedFunction,SpellCheckingInspection
ga('send', 'pageview');
//endregion
